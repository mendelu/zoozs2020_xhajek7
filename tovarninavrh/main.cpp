#include <iostream>

class Mutant {
    int m_sila;
//odebrano public a premisteno
    Mutant(int sila) {
        m_sila = sila;
    }
public:
    int getSila() {
        return m_sila;
    }

    static Mutant* createMutant(int obtiznost){
        Mutant *nepritel = nullptr;

        if (obtiznost == 0) {
            nepritel = new Mutant(10);
        } else if (obtiznost == 1) {
            nepritel = new Mutant(15);
        } else {
            nepritel = new Mutant(30);
        }
        return nepritel;
    }
};

class Hrdina {
    int m_sila;
    std::string m_jmeno;
//odebrano public a premisteno
    Hrdina(int sila, std::string jmeno) {
        m_sila = sila;
        m_jmeno = jmeno;
    }
public:
    int getSila() {
        return m_sila;
    }
    std::string getJmeno(){
        return m_jmeno;
    }

    static Hrdina* createHrdina(int obtiznost, std::string jmeno){
        Hrdina *hrdina = nullptr;

        if (obtiznost == 0) {
            hrdina = new Hrdina(10, jmeno);
        } else if (obtiznost == 1) {
            hrdina = new Hrdina(15, jmeno);
        } else {
            hrdina = new Hrdina(30, jmeno);
        }
        return hrdina;
    }
};

int main() {
    //dotaz na obtiznost
    int obtiznost = 1; // 0 = lehka, 1 = stredni, 2 = obtizna

    //jak se vyhnout tomu, abych toto nemusel opakovat v systemu?
    /* vlozeno do metody Mutant
    Mutant *nepritel = nullptr;
    if (obtiznost == 0) {
        nepritel = new Mutant(10);
    } else if (obtiznost == 1) {
        nepritel = new Mutant(15);
    } else {
        nepritel = new Mutant(30);
    }
*/

    Mutant* nepritel2 = Mutant::createMutant(obtiznost);
    Hrdina* artus = Hrdina::createHrdina(obtiznost, "Artus");

    delete nepritel2;
    delete artus;
    
    return 0;
}

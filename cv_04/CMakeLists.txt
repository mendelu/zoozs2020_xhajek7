cmake_minimum_required(VERSION 3.17)
project(cv_04)

set(CMAKE_CXX_STANDARD 14)

add_executable(cv_04 main.cpp)
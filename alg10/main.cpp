#include <iostream>


using namespace std;

typedef struct {
    string jmeno;
    string prijmeni;
    int rokNarozeni;
    char pohlavi;
    int plat;
} Zamestnanec;
typedef Zamestnanec poleOsob[100];



void nactiZamestnance(Zamestnanec &clovek){
    cout<< "jmeno: ";
    cin>>clovek.jmeno;
    cout<< "prijmeni: ";
    cin>>clovek.prijmeni;
    cout<< "rok narozeni: ";
    cin>>clovek.rokNarozeni;
    cout<< "pohlavi (m/z): ";
    cin>>clovek.pohlavi;
    while(clovek.pohlavi !='m' and clovek.pohlavi !='z'){
        cout<< "pohlavi (m/z): ";
        cin>>clovek.pohlavi;
    }
    cout<< "plat: ";
    cin>>clovek.plat;
}

void vypisZamestnance(Zamestnanec clovek){
    cout<< "jmeno: "<<clovek.jmeno<<endl;
    cout<< "prijmeni: "<<clovek.prijmeni<<endl;
    cout<< "rok narozeni: "<<clovek.rokNarozeni<<endl;
    cout<< "pohlavi (m/z): "<<clovek.pohlavi<<endl;
    cout<< "plat: "<<clovek.plat<<endl;
}



void pridejNaKonec(poleOsob sez, int &pocet){
    nactiZamestnance((sez[pocet++]));
}

void pridejNaZacatek(poleOsob sez, int &pocet){
    for(int i=pocet; i>0;i--){
        sez[i]=sez[i-1];
    }
    nactiZamestnance(sez[0]);
    pocet++;

}
void vypisSeznamLidi(poleOsob sez, int pocet){
    for(int i = 0; i < pocet; i++){
        vypisZamestnance(sez[i]);
        cout << "-----------------------" <<endl;
    }
}
Zamestnanec odeberZeZacatku(poleOsob sez, int &pocet){
    Zamestnanec clovek;
    if (pocet>0){
        clovek = sez[0];
        for(int i=0;i<pocet-1;i++){
            sez[i]=sez[i+1];
        }
        pocet--;
        return clovek;
    }
}

Zamestnanec odeberZKonce(poleOsob sez,int &pocet){
    if(pocet>0){
        Zamestnanec clovek;
        clovek = sez[pocet-1];
        pocet--;
        return clovek;
    }
}

void seradSeznam(poleOsob sez, int pocet){
    for(int i=0; i<pocet-1; i++){
        for(int j=0; j<pocet-i-1; j++){
            if(sez[j+1].prijmeni < sez[j].prijmeni){
                Zamestnanec pom = sez[j+1];
                sez[j+1] = sez[j];
                sez[j] = pom;
            }
        }
    }
}

int menu(){
    int pom;
    cout<< "1 ... Přidej na začátek"<<endl;
    cout<< "2 ... Přidej na konec"<<endl;
    cout<< "3 ... Odeber ze začátku"<<endl;
    cout<< "4 ... Odeber z konce"<<endl;
    cout<< "5 ... Vypiš seznam"<<endl;
    cout<< "6 ... Serařaď seznam"<<endl;
    cout<<endl<< "0 ... Konec programu"<<endl;
    cin>>pom;
    return pom;
}

int main() {
    poleOsob zam;
    int pocet=0;
    pridejNaZacatek(zam,pocet);
    pridejNaZacatek(zam,pocet);
    pridejNaKonec(zam,pocet);
    if(pocet>0){
        cout<<"odebrali jsme: "<< endl;
        vypisZamestnance(odeberZKonce(zam,pocet));
    }
    vypisSeznamLidi(zam,pocet);


    return 0;
}

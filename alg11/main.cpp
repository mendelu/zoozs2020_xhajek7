#include <iostream>
#include <fstream>
using namespace std;

int main() {
    string radek;
    ifstream vstup ("alg_10_nakopii.txt");
    ofstream vystup ("zaloha.txt");
    if (vstup.is_open() and vystup.is_open()){
        while (getline (vstup,radek)){
            vystup<<radek;
        }
        vstup.close();
        vystup.close();
    }
    else cout<<"Nepodarilo se otevrit nektery ze souboru." << endl;
    return 0;
}

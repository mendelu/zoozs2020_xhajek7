//
// Created by zurie on 03.11.2020.
//

#ifndef CV7_CONTAINER_H
#define CV7_CONTAINER_H

#include<iostream>

class Container {
    float m_weight;
    std::string m_owner;

public:
    Container(float weight, std::string owner);

    std::string getOwner();

    float getWeight();
};


#endif //CV7_CONTAINER_H

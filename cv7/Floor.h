//
// Created by zurie on 03.11.2020.
//

#ifndef CV7_FLOOR_H
#define CV7_FLOOR_H

#include <array>
#include <iostream>
#include "Container.h"

class Floor {
    std::string m_label;
    std::array<Container *, 10> m_containers;

public:
    Floor(std::string label);

    void storeContainer(int index, Container *newContainer);

    void removeContainer(int index);

    void printInfo();

    ~Floor();
};


#endif //CV7_FLOOR_H

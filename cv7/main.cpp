#include <iostream>

#include "Container.h"
#include "Floor.h"

int main() {
    Container* container1 = new Container(10, "David");
    Container* container2 = new Container(200, "Mikulas");
    Container* container3 = new Container(300, "Tom");

    Floor* floor1 = new Floor("Prizemi");
    floor1-> storeContainer(2, container1);
    floor1-> storeContainer(5, container2);
    floor1-> storeContainer(8, container3);

    floor1->printInfo();

    delete container1;
    delete container2;
    delete container3;
    delete floor1;
    
    return 0;
}

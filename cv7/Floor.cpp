//
// Created by zurie on 03.11.2020.
//

#include "Floor.h"

// CTRL + ALT + L   automaticke formatovani

Floor::Floor(std::string label) {
    m_label = label;
    // & pouzivame, protoze chceme menit pole
    for (Container *&container:m_containers) {
        container = nullptr;
    }
}

void Floor::storeContainer(int index, Container *newContainer) {
    if ((index >= 0) and (index < 10)) {
        // index je ok
        if (m_containers.at(index) == nullptr) {
            //nic tu zatim neni
            m_containers.at(index) = newContainer;
        } else {
            std::cout << "There is container at index " << index << " ! \n";

        }
    } else {
        std::cout << "You are storing out of range! " << std::endl;
    }
}

void Floor::removeContainer(int index) {
    if ((index >= 0) and (index < 10)) {
        //index je ok
        if (m_containers.at(index) != nullptr) {
            //neco tu je -> muzeme to vymazat
            m_containers.at(index) = nullptr;
        } else {
            std::cout << "There is no container at index " << index << std::endl;
        }
    } else {
        std::cout << "You are removing out of range! " << std::endl;
    }
}



void Floor::printInfo() {
    std::cout << "Floor " << m_label << std::endl;
    for(Container *position:m_containers){
        if(position != nullptr) {
            std::cout << position->getOwner() << ": " << position->getWeight() << std::endl;

        }else{
                std::cout << "Position is empty!" << std::endl;

            }
        }
    }



Floor::~Floor() {
    for (auto* container:m_containers) {
        delete (container);
    }
}
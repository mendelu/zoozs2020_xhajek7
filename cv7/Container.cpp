//
// Created by zurie on 03.11.2020.
//

#include "Container.h"

Container::Container(float weight, std::string owner) {
    m_weight = weight;
    m_owner = owner;
}

std::string Container::getOwner() {
    return m_owner;
}

float Container::getWeight(){
        return m_weight;
};

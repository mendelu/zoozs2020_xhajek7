//
// Created by zurie on 02.11.2020.
//

#ifndef VELKE_PROJEKTY_COURSE_H
#define VELKE_PROJEKTY_COURSE_H

#include <iostream>

class course {
    std::string m_title;
    int m_credits;

public:
    course(std::string title, int credits);
    void printInfo();
};


#endif //VELKE_PROJEKTY_COURSE_H

//
// Created by zurie on 02.11.2020.
//

#ifndef VELKE_PROJEKTY_TEACHER_H
#define VELKE_PROJEKTY_TEACHER_H

#include <vector>
#include "course.h"

class Teacher {
    std::vector<course*> m_courses;
public:
    Teacher();
    void addCourse(course* newCourse);
    void printCourses();
};


#endif //VELKE_PROJEKTY_TEACHER_H

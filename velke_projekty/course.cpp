//
// Created by zurie on 02.11.2020.
//

#include "course.h"

course::course(std::string title, int credits){
    m_title = title;
    m_credits = credits;
}

void course::printInfo(){
    std::cout << "Title: " << m_title << std::endl;
    std::cout << "Credits: " << m_credits << std::endl;
}
#include <iostream>
using namespace std;

class Brneni {
    int m_bonusObrany;
public:
    Brneni(int bonusObrany){

    m_bonusObrany = bonusObrany;
}

    int getBonusObrany() {
        return m_bonusObrany;
    }
};

class Rytir{
    int m_obrana;
    Brneni* m_neseneBrneni;

public:
    Rytir(int obrana){
        m_obrana = obrana;
        m_neseneBrneni = nullptr; //0;
    }

    void seberBrneni(Brneni* brneni){
        if (m_neseneBrneni == nullptr){
            m_neseneBrneni = brneni;
        }else {
            cout << "Promin, nejdriv zahod nesene brneni: " << endl;

        }
    }

    void zahodBrneni(){
        m_neseneBrneni = nullptr;

    }

    int getObrana(){
        if (m_neseneBrneni == nullptr){
            return m_obrana;
        }else{
            return m_obrana + m_neseneBrneni->getBonusObrany();
        }
    }

};

int main(){
    Rytir* artus = new Rytir(50);
    Brneni* platove = new Brneni(20);
    Brneni* krouzkove = new Brneni(20);

    cout << "Obrana: " << artus->getObrana() << endl;
    artus->seberBrneni(platove);
    artus->seberBrneni(krouzkove);

    cout << "Obrana: " << artus->getObrana() << endl;
    artus->zahodBrneni();
    cout << "Obrana: " << artus->getObrana() << endl;

    delete krouzkove;
    delete platove;
    delete artus;
    return 0;
}

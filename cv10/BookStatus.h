//
// Created by zurie on 24.11.2020.
//

#ifndef CV10_BOOKSTATUS_H
#define CV10_BOOKSTATUS_H

enum class BookStatus{
    LOST, AVAILABLE, BORROWED
};


#endif //CV10_BOOKSTATUS_H

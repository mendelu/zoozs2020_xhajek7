//
// Created by zurie on 24.11.2020.
//

#ifndef CV10_LIBRARY_H
#define CV10_LIBRARY_H

#include <iostream>
#include <vector>
#include "Book.h"

class Library {
    static std::vector<Book *> s_books;

    Library(){};

public:

    static Book* createBook(std::string author, std::string title, int year);

    static void removeBookById(int bookId);

    static void printStatistics();

private:
    static std::vector<Book *> getBooksByStatus(BookStatus status);

};


#endif //CV10_LIBRARY_H

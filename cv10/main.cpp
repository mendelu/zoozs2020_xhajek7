#include <iostream>

#include "Library.h"

int main() {
    //Library* library = new Library() ....nelze, trida library je private

    Library::createBook("James Patterson", "Kokoti", 2004);
    Library::createBook("James Patterson", "Lempli", 2006);
    Library::createBook("Arnost Lemra", "Posuci", 2017);

    Library::printStatistics();

    Library::removeBookById(10);

    Library::printStatistics();

    return 0;
}

//
// Created by zurie on 24.11.2020.
//

#ifndef CV10_BOOK_H
#define CV10_BOOK_H

#include <iostream>
#include "BookStatus.h"

class Book {
    int m_id;
    std::string m_author;
    std::string m_title;
    int m_year;
    BookStatus m_status;
    static int s_instances;

public:
    Book(std::string author, std::string title, int year);

    int getId();

    BookStatus getStatus();
};


#endif //CV10_BOOK_H

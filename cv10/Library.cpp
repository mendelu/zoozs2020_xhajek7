//
// Created by zurie on 24.11.2020.
//

#include "Library.h"

std::vector<Book *> Library::s_books = {};

Book * Library::createBook(std::string author, std::string title, int year) {
    Book *newBook = new Book(author, title, year);
    s_books.push_back(newBook);
    return newBook;
}

void Library::removeBookById(int bookId) {
    /*
     int index = bookId - 1;
      Book* book = s_books.at(index);

      if(book == nullptr){
          std::cout << "Book with ID " << bookId << "was not found " << std::endl;
      }else{
          s_books.erase(s_books.begin() + index);
      }
    */
    bool wasFound = false;
    int index = 0;
    for (auto book:s_books) {
        index++;
        if (book->getId() == bookId) {
            wasFound = true;
            delete (book);
            s_books.erase(s_books.begin() + index);
            break;
        }
    }

        if (!wasFound) {
            std::cout << "Book with ID " << bookId << " was not found " << std::endl;
        }
    }
void Library::printStatistics() {
    std::cout << "=== Statistics ===" << std::endl;
    std::cout << "Total books count: " << s_books.size() << std::endl;
    std::cout << "Available books count: " << getBooksByStatus(BookStatus::AVAILABLE).size() << std::endl;
    std::cout << "Borrowed books count: " << getBooksByStatus(BookStatus::BORROWED).size() << std::endl;
    std::cout << "Lost books count: " << getBooksByStatus(BookStatus::LOST).size() << std::endl;
}

std::vector<Book *> Library::getBooksByStatus(BookStatus status) {
    std::vector<Book*> foundBooks = {};

    for (Book* book:s_books) {
        if(book->getStatus() == status){
            foundBooks.push_back(book);
        }
    }

    return foundBooks;
}

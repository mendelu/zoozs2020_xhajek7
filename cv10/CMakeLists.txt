cmake_minimum_required(VERSION 3.17)
project(cv10)

set(CMAKE_CXX_STANDARD 14)

add_executable(cv10 main.cpp Book.cpp Book.h Library.cpp Library.h BookStatus.h)
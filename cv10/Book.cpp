//
// Created by zurie on 24.11.2020.
//

#include "Book.h"

int Book::s_instances = 0;

Book::Book(std::string author, std::string title, int year) {
    m_author = author;
    m_title = title;
    m_year = year;
    m_status = BookStatus::AVAILABLE;
    s_instances+= 1;
    // s_instances ++
    m_id = s_instances;

}
int Book::getId() {
    return m_id;
}

BookStatus Book::getStatus(){
    return m_status;
}

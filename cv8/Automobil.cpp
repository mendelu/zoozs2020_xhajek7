//
// Created by zurie on 10.11.2020.
//

#include "Automobil.h"

Automobil::Automobil(std::string model, int cena) {
    m_model = model;
    m_cena = cena;
    m_motor = new Motor(50, 50000);
}

Automobil::Automobil(std::string model, int cena, float vykon, int cena_motoru) {
    m_model = model;
    m_cena = cena;
    m_motor = new Motor(vykon, cena_motoru);
}

int Automobil::getCena() {
    return m_cena + m_motor->getCena();
}

std::string Automobil::getModel() {
    return m_model;
}

float Automobil::getPocetKoni() {
    return m_motor->getPocetKoni();
}

void Automobil::printInfo() {
    std::cout << "Automobil" << getModel() << " s cenou" << getCena();
    std::cout << " s motorem o vykonu" << m_motor->getPocetKoni() << std::endl;
}

Automobil::~Automobil() {
    delete m_motor;
}


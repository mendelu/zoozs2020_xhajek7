//
// Created by zurie on 10.11.2020.
//

#include "Motor.h"


Motor::Motor(float vykon, int cena) {
    m_vykon = vykon;
    m_cena = cena;
}

int Motor::getCena() {
    return m_cena;
}

float Motor::getVykon() {
    return m_vykon;
}

float Motor::getPocetKoni() {
    return m_vykon * 1.36;
}
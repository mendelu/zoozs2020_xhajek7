//
// Created by zurie on 10.11.2020.
//


#include "Autosalon.h"

void Autosalon::vypisAuta() {
    for (Automobil *automobil : m_automobily) {
        automobil->printInfo();
    }
}

int Autosalon::getCelkovaCena() {
    int celkova_cena = 0;
    for (Automobil *automobil : m_automobily) {
        celkova_cena += automobil->getCena();
    }
    return celkova_cena;
}

void Autosalon::pridejAutomobil(Automobil *automobil) {
    m_automobily.push_back(automobil);
}

void Autosalon::pridejAutomobil(std::string model, int cena) {
    Automobil *automobil = new Automobil(model, cena);
    m_automobily.push_back(automobil);
}

void Autosalon::pridejAutomobil(std::string model, int cena, float vykon, int cena_motoru) {
    Automobil *automobil = new Automobil(model, cena, vykon, cena_motoru);
    m_automobily.push_back(automobil);
}

void Autosalon::odeberAutomobil(int pozice) {
    if(m_automobily.size()> pozice) {
        m_automobily.erase(m_automobily.begin() + pozice);
    }
    else{
        std::cout << "Na pozici neni zadne auto" << std::endl;
    }
}

 Automobil* Autosalon::getPosledniPridanyAutomobil(){
    return m_automobily.back();
}


//
// Created by zurie on 10.11.2020.
//

#ifndef CV8_MOTOR_H
#define CV8_MOTOR_H


class Motor {
private:
    float m_vykon;
    int m_cena;
public:
    Motor(float vykon, int cena);
    int getCena();
    float getVykon();
    float getPocetKoni();
};


#endif //CV8_MOTOR_H

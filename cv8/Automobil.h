//
// Created by zurie on 10.11.2020.
//

#ifndef CV8_AUTOMOBIL_H
#define CV8_AUTOMOBIL_H

#include <iostream>
#include "Motor.h"

class Automobil {
private:
    std::string m_model;
    int m_cena;
    Motor *m_motor;
public:
    Automobil(std::string model, int cena);

    Automobil(std::string model, int cena, float vykon, int cena_motoru);

    ~Automobil();

    int getCena();

    float getPocetKoni();

    std::string getModel();

    void printInfo();

};


#endif //CV8_AUTOMOBIL_H

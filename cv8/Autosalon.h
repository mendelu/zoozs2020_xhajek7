//
// Created by zurie on 10.11.2020.
//

#ifndef CV8_AUTOSALON_H
#define CV8_AUTOSALON_H

#include <vector>
#include "Automobil.h"

class Autosalon {
private:
    std::vector<Automobil *> m_automobily;
public:
    void vypisAuta();

    int getCelkovaCena();

    void pridejAutomobil(Automobil *automobil);

    void pridejAutomobil(std::string model, int cena);

    void pridejAutomobil(std::string model, int cena, float vykon, int cena_motoru);

    void odeberAutomobil(int pozice);

    Automobil* getPosledniPridanyAutomobil();
};


#endif //CV8_AUTOSALON_H

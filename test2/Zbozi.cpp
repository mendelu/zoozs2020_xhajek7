//
// Created by zurie on 01.12.2020.
//

#include "Zbozi.h"

Zbozi::Zbozi (std::string jmeno,float cena) {
    m_jmeno = jmeno;
    m_cena = cena;
};

void Zbozi::printInfo() {
    std::cout << "Nazev zbozi je: " << getJmeno() << std::endl;
    std::cout << "Cena zbozi je: " << getCena() << std::endl;
}

std::string Zbozi::getJmeno() {
    return m_jmeno;
}
float Zbozi::getCena() {
    return m_cena*1.22;
}


//
// Created by zurie on 01.12.2020.
//

#include "Potravina.h"

Potravina::Potravina(std::string jmeno, float cena, std::string datumTrvanlivosti):Zbozi(jmeno,cena) {
    m_jmeno = jmeno;
    m_cena = cena;
    m_datumTrvanlivosti = datumTrvanlivosti;
};

void Potravina::printInfo() {
    std::cout << "Nazev potraviny je: " << getJmeno() << std::endl;
    std::cout << "Cena potraviny je: " << getCena() << std::endl;
}
std::string Potravina::getJmeno() {
    return m_jmeno;
}

float Potravina::getCena() {
    return m_cena*1.15;
}



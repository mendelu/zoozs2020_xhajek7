//
// Created by zurie on 01.12.2020.
//

#ifndef TEST2_POTRAVINA_H
#define TEST2_POTRAVINA_H
#include "Zbozi.h"


class Potravina : public Zbozi {
    std::string m_datumTrvanlivosti;
    std::string m_jmeno;
    float m_cena;
public:
    Potravina(std::string jmeno, float cena, std::string datumTrvanlivosti);

    void printInfo();
    std::string getJmeno();
    float getCena();

};


#endif //TEST2_POTRAVINA_H

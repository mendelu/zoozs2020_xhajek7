//
// Created by zurie on 01.12.2020.
//

#include "Kosik.h"

Kosik::Kosik() {

}

void Kosik::addZbozi(std::string jmeno, float cena) {
    Zbozi* novyZbozi = new Zbozi(jmeno,cena);
    m_zboziVKosiku.push_back(novyZbozi);
}


void Kosik::addPotraviny(std::string jmeno, float cena, std::string datumTrvanlivosti) {
    Potravina* novaPotravina = new Potravina(jmeno,cena,datumTrvanlivosti);
}

void Kosik::odeberZbozi() {
}

void Kosik::printZbozi() {
    for(auto zbozi:m_zboziVKosiku){
        zbozi->printInfo();
    }
}
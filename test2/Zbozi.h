//
// Created by zurie on 01.12.2020.
//

#ifndef TEST2_ZBOZI_H
#define TEST2_ZBOZI_H
#include <iostream>

class Zbozi {
    std::string m_jmeno;
    float m_cena;

public:
    Zbozi(std::string jmeno,float cena);

    void printInfo();
    std::string getJmeno();
    float getCena();
};


#endif //TEST2_ZBOZI_H

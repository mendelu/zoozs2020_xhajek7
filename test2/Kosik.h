//
// Created by zurie on 01.12.2020.
//

#ifndef TEST2_KOSIK_H
#define TEST2_KOSIK_H
#include "Zbozi.h"
#include "Potravina.h"
#include <vector>

class Kosik {
    std::vector<Zbozi*> m_zboziVKosiku;
public:
    Kosik();
    void addZbozi(std::string jmeno, float cena);
    void addPotraviny(std::string jmeno, float cena, std::string datumTrvanlivosti);
    void odeberZbozi();
    void printZbozi();
};


#endif //TEST2_KOSIK_H

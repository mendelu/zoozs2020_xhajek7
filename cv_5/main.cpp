#include <iostream>
using namespace std;

class Auto
{
    int m_najetoKm;
    int m_cenaZaPujceniNaDen;
    int m_celkovyVydelek;
    int m_citacOpotrebeni = 0;
    int m_limitProSnizeniCeny = 10000;

private:
    int CenaVypujceni(int pocetDni)
    {
        return (pocetDni * m_cenaZaPujceniNaDen);
    };
    void opotrebeniAuta(int najetoKm)
    {
        m_najetoKm += najetoKm;
        m_citacOpotrebeni += najetoKm;
        if (m_citacOpotrebeni >= m_limitProSnizeniCeny)
        {
            m_cenaZaPujceniNaDen -= m_cenaZaPujceniNaDen / 10;
            m_citacOpotrebeni = 0;
        }
    };
public:
    Auto(int najetoKm, int cenaZaPujceniNaDen, int celkovyVydelek)
    {
        m_najetoKm = najetoKm;
        m_cenaZaPujceniNaDen = cenaZaPujceniNaDen;
        m_celkovyVydelek = celkovyVydelek;
    };
    void pridatZaznam(int pocetDni, int najetoKm)
    {
        m_celkovyVydelek += CenaVypujceni(pocetDni);
        opotrebeniAuta(najetoKm);
    };

    void printInfo()
    {
        cout << "Najeto Km: " << m_najetoKm << endl;
        cout << "Cena za pujceni na 1 den: " << m_cenaZaPujceniNaDen << endl;
        cout << "Toto auto jiz vydelalo: " << m_celkovyVydelek << endl;
    };



};

int main()
{
    Auto* auto1 = new Auto(0, 1000, 0);
    auto1->pridatZaznam(2, 1300);
    auto1->printInfo();

    auto1->pridatZaznam(10, 9500);
    auto1->printInfo();

    auto1->pridatZaznam(8, 1200);
    auto1->printInfo();

    auto1->pridatZaznam(10, 9000);
    auto1->printInfo();
    return(0);
};